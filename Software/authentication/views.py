from django.shortcuts import render,redirect
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.contrib import messages
from django.contrib.auth import login as auth_login


def login(request):
    if request.method == "POST":
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username,password=password)
        if user is not None:
            auth_login(request,user)
            messages.info(request,"Login Succesfull.")
            return redirect("/user/")
        else:
            messages.error(request,"Invalide credentials!")
    return render(request,"auth/login.html")

def logout(request):
    messages.info(request,"Succesfully logout.")
    return redirect("/")

def signup(request):
    #User.objects.create_user(username,email,password)
    #user.save()
    return render(request,"auth/signup.html")