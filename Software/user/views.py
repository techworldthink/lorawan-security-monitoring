from django.shortcuts import render,redirect
from loradata import views as loradata_views
from devicemanage import models as device_models
from datetime import datetime


# convert string type date to specific datetime object
def string_to_timedate(datetime_str):
    datetime_str = datetime_str.split(".")[0]
    timeobj =datetime.strptime(datetime_str, "%Y-%m-%dT%H:%M:%S")
    return timeobj.strftime("%b. %d, %Y, %H:%M ") + timeobj.strftime("%p").lower()

###############################################################################################333


# home page with navbar
def home(request):
   return render(request,"user/home.html")

# user dashbaord 
def dashboard(request):
   risk_status,risk_counts = loradata_views.get_device_risk_status()
   device_count = device_models.DeviceData.objects.all().count()
   return render(request,"user/dashboard.html",{
      "risk_status" : risk_status,
      "device_count" : device_count,
      "risk_counts" : risk_counts
      })

# GET - device data send along with device manage page
# POST - collect device info from chirpstack and store it into DeviceData table
def device_manage(request):
   if request.method == "POST":
      device_info = loradata_views.get_device_info()
      for each in device_info:
         deviceObj = device_models.DeviceData(
             dev_eui = each['devEUI'],
             last_seen_at = string_to_timedate(each['lastSeenAt']),
             dev_addr = each['devAddr'] 
         )
         # If the device already exist in table,ignore it
         # else store it on databse
         try:
            device_models.DeviceData.objects.get(dev_eui = each['devEUI'])
         except:
            deviceObj.save()
   device_data = device_models.DeviceData.objects.all()
   total_device = len(device_data)
   active_device = len(device_models.DeviceData.objects.filter(monitor_status=1))
   device_status = {
      "total_device" : total_device,
      "active_device" :active_device ,
      "deactive_device" :  total_device - active_device
   }
   return render(request,"user/device/device.html",{
      "devices": device_data,
      "device_status" : device_status
      })


# toggle the device monitoring status
def device_status_toggle(request):
   if request.method == "POST":
      status = request.POST.get('d_status')
      device_eui = request.POST.get('d_eui')
      deviceObj = device_models.DeviceData.objects.get(dev_eui = device_eui)
      deviceObj.monitor_status = 1 if status=='0' else 0
      deviceObj.updated_date = datetime.now()
      deviceObj.save()
   return redirect('device_manage')


# get the latest device info from chirpstack
# update the last seen of existing devices within the table
def update_device_lastseen(request):
   new_device_info = loradata_views.get_device_info()
   existing_device_data = list(device_models.DeviceData.objects.values_list('dev_eui', flat=True))
   for device in new_device_info:
      if device['devEUI'] in existing_device_data:
         print(device['devEUI'])
         try:
             deviceObj = device_models.DeviceData.objects.get(dev_eui = device['devEUI'])
             deviceObj.last_seen_at = string_to_timedate(device['lastSeenAt']) 
             deviceObj.updated_date = datetime.now()
             deviceObj.save()
         except:
            pass
   return redirect('device_manage')