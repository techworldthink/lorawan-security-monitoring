from django.apps import AppConfig


class LoradataConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'loradata'
