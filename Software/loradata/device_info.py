import requests
import datetime 

device_data = []

# LORASERVER -  credentials & urls
email_list = ["test@icfoss.org"]
password_list = ["mypassword"]
LORASERVERURL_LIST=["https://loraserver.icfoss.org"]
GATEWAYS = []


def string_to_timedate(datetime_str):
    return datetime.datetime.strptime(datetime_str, "%Y-%m-%dT%H:%M:%S.%fZ")

# get previous datetime with format (yyyy-MM-ddTHH:mm:ss.fZ)
def get_crnt_timedate():
    crnt_time = datetime.datetime.now(datetime.timezone.utc)
    formatted_date = crnt_time.strftime("%Y-%m-%dT%H:%M:%S.%fZ")
    return string_to_timedate(formatted_date)
    
def add_device_info(url,LORASERVERAP):
    global device_data
    headers = { "Accept": "application/json","Grpc-Metadata-Authorization": "Bearer "+LORASERVERAP}
    response = requests.get(url+"/api/devices?limit=1000", headers=headers, stream=False)
    response = response.json()['result']
    for device in response:
        if device['lastSeenAt'] is not None:
            time_diff_s = (get_crnt_timedate() - string_to_timedate(device['lastSeenAt'])).total_seconds() 
            time_diff_day = time_diff_s // (60*60*24)
            if(time_diff_day <= 60):
                response = requests.get(url+"/api/devices/"+device['devEUI']+"/activation", headers=headers, stream=False)
                response = response.json()['deviceActivation']
                device_info = {
                    "devEUI"     : device['devEUI'],
                    "lastSeenAt" : device['lastSeenAt'],
                    "devAddr"    : response['devAddr']
                }
                device_data.append(device_info)
    return device_data
    

# get jwt token
def apilogin(email,password,url):
  url=url+'/api/internal/login'
  myobj = '{"password": "'+password+'","email": "'+email+'"}'
  headers = {'content-type': 'application/json', 'Accept-Charset': 'UTF-8'}
  x = requests.post(url, data = myobj, headers=headers)
  data = x.json()
  return data['jwt']


def get_device_info():
    global device_data
    for x in range(len(LORASERVERURL_LIST)):
        print("--START URL : " + LORASERVERURL_LIST[x])
        LORASERVERURL = LORASERVERURL_LIST[x]
        email=email_list[x]
        password=password_list[x]
        LORASERVERAP=apilogin(email,password,LORASERVERURL)
        add_device_info(LORASERVERURL,LORASERVERAP)
        
    return device_data

           
     
