# Secure Lora v1.0
Security issues are common in any communication system. Several critical security flaws are present in the case of Lora's communication. To prevent this, we must monitor the same as the first stage. Chirpstack provides an API to capture the live frame, which includes uplink and downlinks. Using these captured frames, we can analyse the security threats that we currently face. We created security monitoring scripts for these types of vulnerabilities.

![image1](Images/dashboard.png)

# Key Features
  
  - This system is able to monitor the following attacks:
    - Packet flooding
    - Join request replay
    - DevNonce reset
    - Device reset
    - Packet loss(WIP)
    - Active/Inactive devices
  - Selective monitoring of devices 

# Getting Started

- Set up credentials in the Cronjobs/crontasks.py file and add them to the cron task.
- Run the Django application
  - python manage.py runserver
- Make sure the credentials in the following files are set before accessing the webpages.
  - Software/loradata/dbconnector.py
  - Software/loradata/device_info.py

# Prerequisites
  - Python 3
  - influxdb python library
  - django

# Contributing

# License

# Troubleshooting

# Acknowledgments

# Contribute to development

# Changelog
